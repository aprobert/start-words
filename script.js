import wordleList from "./allWordsList.js";
//Get full list of starter words
var words5 = [];
var allStartWords = [];
var randomWord;
var vowels = ["a", "e", "i", "o", "u"];
var vowels4 = [];
var vowels3 = [];

function getRandomWord() {
    var rand = Math.floor(Math.random() * allStartWords.length)
    randomWord = allStartWords[rand];
    console.log(randomWord);
}

function getWords(wordsArr) {
    for (var word of wordsArr) {
        if (word.length == 5) {
            words5.push(word)
        }
    }
    for (var w of words5) {
        var count = 0;
        for (var v of vowels) {
            if(w.includes(v)) {
                count++;
            }
        }
        if(count >= 4) {
            vowels4.push(w);
        }
        if(count == 3) {
            vowels3.push(w);
        }
        if(count >= 3) {
            allStartWords.push(w)
        }
    }
    
    //getRandomWord()
    
} 
function showWords() {
    var v4 = document.getElementById('v4');
    var v3 = document.getElementById('v3');
    for(var word4 of vowels4) {
        var newEl = document.createElement('li');
        newEl.style.fontSize = "medium";
        newEl.style.fontFamily = "Poppins";
        newEl.innerHTML = word4;
        v4.appendChild(newEl);
    }
    for(var word3 of vowels3) {
        var newEl = document.createElement('li');
        newEl.innerHTML = word3;
        newEl.style.fontSize = "small";
        newEl.style.fontFamily = "Poppins";
        v3.appendChild(newEl);
    }

}

// GET WORDS WITH 3 OR MORE VOWELS
//Loop through each 5 letter word
//For each 5 letter word loop through the vowels array to see if it includes that vowel
//If it includes that vowel add 1 and remove it from the array
//Continue checking for each vowel 

//fetch("https://raw.githubusercontent.com/dwyl/english-words/master/words_dictionary.json")

// fetch("./valid-wordle-words.txt")
// .then(function(resp) {
//     return resp.json()
// })
// .then(function(data) {
//     return data
// })
// .then(function(obj) {
//     var keys = Object.keys(obj);
//     getWords(keys);
//     showWords()
//     return fetch(`https://www.dictionaryapi.com/api/v3/references/collegiate/json/${randomWord}?key=f9c0eace-9ae3-4d23-a206-1ef3adb35521`)  
// })
// .then(function(resp2) {
//     return resp2.json()
// })
// .then(function(wordData) {
//     console.log(wordData[0].shortdef);
//     return wordData
// })

getWords(wordleList);

showWords();

function dailyWord() {
    getRandomWord();
    fetch(`https://www.dictionaryapi.com/api/v3/references/collegiate/json/${randomWord}?key=f9c0eace-9ae3-4d23-a206-1ef3adb35521`)  
    .then(function(dictResp) {
        return dictResp.json();
    })
    .then(function(wordData) {
        var definition = wordData[0].shortdef;
        if(definition) {
            console.log(definition);
            var randWord = document.querySelector("#randWord");
            randWord.innerHTML = randomWord;
            var wordDef = document.querySelector("#wordDef");
            wordDef.innerHTML = definition[0]
            return true
        }
        else {
            console.log("could not find");
            dailyWord();
        }
    })
}

dailyWord()

